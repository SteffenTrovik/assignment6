import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_SUCCESS,
    loginErrorAction,
    loginSuccessAction
} from "../actions/loginActions";
import {LoginAPI} from "../../components/Login/LoginAPI";
import {sessionSetAction} from "../actions/sessionActions";

export const loginMiddleware = ({dispatch}) => next => action => {

    next(action)

    /// attempts to log in user and further dispatches successful login or the loginerroraction
    if(action.type===ACTION_LOGIN_ATTEMPTING){
        LoginAPI.login(action.payload)
            .then(profile => {
                dispatch(loginSuccessAction(profile))
            })
            .catch(error => {
                dispatch(loginErrorAction(error.message))
            })
    }

    /// sets the session upon successful login
    if(action.type === ACTION_LOGIN_SUCCESS){
        dispatch(sessionSetAction(action.payload))
    }
}