import {
    ACTION_TRANSLATE_ATTEMPTING, ACTION_TRANSLATE_CLEAR_ATTEMPTING,
    ACTION_TRANSLATE_SUCCESS,
    translateErrorAction,
    translateSuccessAction
} from "../actions/translateActions";
import {TranslationsAPI} from "../../components/Translations/TranslationsAPI";
import {sessionClearTranslationsAction} from "../actions/sessionActions";


export const translateMiddleware = ({dispatch}) => next => action => {

    next(action)

    /// tries to translate/add the translation to the database and dispatches result action
    if(action.type===ACTION_TRANSLATE_ATTEMPTING){
        TranslationsAPI.translate(action.payload)
            .then(profile => {
                dispatch(translateSuccessAction(profile))
            })
            .catch(error => {
                dispatch(translateErrorAction(error.message))
            })
    }

    /// tries to clear the translations from the database and dispatches result action
    if(action.type===ACTION_TRANSLATE_CLEAR_ATTEMPTING){
        TranslationsAPI.clear(action.payload)
            .then(profile => {
                dispatch(translateSuccessAction(profile))
            })
            .catch(error => {
                dispatch(translateErrorAction(error.message))
            })
    }

    /// clears translations from session upon following translation database clear
    if(action.type === ACTION_TRANSLATE_SUCCESS){
        dispatch(sessionClearTranslationsAction())

    }
}