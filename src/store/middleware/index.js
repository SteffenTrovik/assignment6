
import {loginMiddleware} from "./loginMiddleware";
import {applyMiddleware} from "redux";
import {sessionMiddleware} from "./sessionMiddleware";
import {translateMiddleware} from "./translateMiddleware";

/// adds the necessary middleware
export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    translateMiddleware
)