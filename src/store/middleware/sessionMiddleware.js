import {
    ACTION_SESSION_CLEAR,
    ACTION_SESSION_INIT,
    ACTION_SESSION_SET,
    sessionSetAction
} from "../actions/sessionActions";

export const sessionMiddleware = ({ dispatch }) => next => action => {

    next(action)

    /// checks localstorage for the user and sets the session if found
    if(action.type===ACTION_SESSION_INIT){
        const storedSession = localStorage.getItem('lit-ss')
        if(!storedSession){
            return
        }
        const session = (JSON.parse(storedSession))
        dispatch(sessionSetAction(session))
        //dispatch(loginAttemptAction(session))
    }

    /// saves the profile in the local storage
    if (action.type === ACTION_SESSION_SET) {
        localStorage.setItem('lit-ss',JSON.stringify(action.payload))
    }

    /// clears the session
    if (action.type === ACTION_SESSION_CLEAR) {
        localStorage.clear()
    }

}