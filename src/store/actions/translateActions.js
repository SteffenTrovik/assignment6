
export const ACTION_TRANSLATE_ATTEMPTING = '[translate] ATTEMPT'
export const ACTION_TRANSLATE_SUCCESS = '[translate] SUCCESS'
export const ACTION_TRANSLATE_ERROR = '[translate] ERROR'
export const ACTION_TRANSLATE_CLEAR_ATTEMPTING = '[translateClear] ATTEMPT'

export const translateAttemptAction = translation => ({
    type: ACTION_TRANSLATE_ATTEMPTING,
    payload: translation
})

export const translateSuccessAction = profile => ({
    type: ACTION_TRANSLATE_SUCCESS,
    payload: profile
})

export const translateErrorAction = error => ({
    type: ACTION_TRANSLATE_ERROR,
    payload: error
})

export const translateClearAttempting = () => ({
    type: ACTION_TRANSLATE_CLEAR_ATTEMPTING
})