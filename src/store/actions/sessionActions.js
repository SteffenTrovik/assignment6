
export const ACTION_SESSION_SET = '[session] SET'
export const ACTION_SESSION_INIT= '[session] INIT'
export const ACTION_SESSION_TRANSLATE_ADD= '[session] ADD TRANSLATION'
export const ACTION_SESSION_CLEAR='[session] CLEAR SESSION'
export const ACTION_SESSION_CLEAR_TRANSLATIONS='[session] CLEAR TRANSLATIONS'

export const sessionSetAction = profile => ({
    type: ACTION_SESSION_SET,
    payload: profile
})

export const sessionClearAction = () => ({
    type: ACTION_SESSION_CLEAR
})

export const sessionClearTranslationsAction = () => ({
    type: ACTION_SESSION_CLEAR_TRANSLATIONS
})

export const sessionInitAction = () => ({
    type: ACTION_SESSION_INIT
})