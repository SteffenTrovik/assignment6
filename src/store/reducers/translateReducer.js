import {
    ACTION_TRANSLATE_ATTEMPTING,
    ACTION_TRANSLATE_CLEAR_ATTEMPTING,
    ACTION_TRANSLATE_ERROR,
    ACTION_TRANSLATE_SUCCESS
} from "../actions/translateActions";

const initialState = {
    translation: '',
    translateAttempting: false,
    translateError: ''
}

/// sets the state to the correct data depending on the action
export const translateReducer = (state = initialState, action) => {

    switch(action.type){
        case ACTION_TRANSLATE_ATTEMPTING:
            return {
                ...state,
                translation: action.payload,
                translateAttempting: true,
                translateError: ''
            }
        case ACTION_TRANSLATE_SUCCESS:
            return{
                ...state
            }
        case ACTION_TRANSLATE_ERROR:
            return{
                ...state,
                translateAttempting: false,
                translateError: action.payload
            }
        case ACTION_TRANSLATE_CLEAR_ATTEMPTING:
            return{
                ...initialState
            }
        default:
            return state
    }



}