import {ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_ERROR, ACTION_LOGIN_SUCCESS} from "../actions/loginActions";

const intialState = {
    loginAttempting: false,
    loginError: ''
}
/// sets the state to the correct data depending on the action
export const loginReducer = (state = intialState, action) => {

    switch(action.type){
        case ACTION_LOGIN_ATTEMPTING:
            return {
                ...state,
                loginAttempting: true,
                loginError: ''
            }
        case ACTION_LOGIN_SUCCESS:
            return{
                ...intialState
            }
        case ACTION_LOGIN_ERROR:
            return{
                ...state,
                loginAttempting: false,
                loginError: action.payload
            }
        default:
            return state
    }

}