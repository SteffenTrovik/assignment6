import {ACTION_SESSION_SET, ACTION_SESSION_TRANSLATE_ADD} from "../actions/sessionActions";
import {ACTION_TRANSLATE_CLEAR_ATTEMPTING} from "../actions/translateActions";

const initialState = {
    id: '',
    username: '',
    translations: [],
    loggedIn: false
}

/// sets the state to the correct data depending on the action
export const sessionReducer = (state = initialState,action) => {

    switch(action.type){

        case ACTION_SESSION_SET:
            return{
                ...action.payload,
                loggedIn: true
            }


        case ACTION_SESSION_TRANSLATE_ADD:
            return{
                ...state,
                translations: [...state.translations,action.payload],
            }

        case ACTION_TRANSLATE_CLEAR_ATTEMPTING:
            return{
                ...state,
                translations: []
            }

        default:
            return state
    }
}