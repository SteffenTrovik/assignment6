import './App.css';
import {
    BrowserRouter,
    Switch,
    Route,
} from 'react-router-dom'
import AppContainer from "./hoc/AppContainer";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import Translations from "./components/Translations/Translations";
import Navbar from "./components/Navbar";
import Profile from "./components/Profile/Profile";

function App() {
    return (
        <BrowserRouter>
            <AppContainer>
                <div className="App">
                    <Navbar/>
                    <Switch>
                        <Route path="/" exact component={Login} />
                        <Route path ="/translations" component ={Translations}/>
                        <Route path ="/profile" component ={Profile}/>
                        <Route path="*" component={NotFound}/>
                    </Switch>
                </div>
            </AppContainer>
        </BrowserRouter>
    );
}

export default App;
