
const apiURL = 'https://sat-noroff-api.herokuapp.com'
const apiKey = 'fHinlLXDHB0GKcGgHurrnlIb1wsrQK8qsYBHUJOdAjjV3V4M6cBRf10Ea5atPn7vCA+IFK97AgQRGmoTWQI9MQ=='

export const TranslationsAPI = {

    ///Stores the translation in the session and in the database
    async translate(translation){
        const storedSession = localStorage.getItem('lit-ss')
        const session = (JSON.parse(storedSession))
        session.translations=[...session.translations,translation]
        localStorage.setItem('lit-ss',JSON.stringify(session))

        return await fetch(`${apiURL}/translations/${session.id}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new translations to add to user with id 1
                translations: session.translations
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update translations history')
                }
                return response.json()
            })
            .then(updatedUser => {
                return updatedUser
            })
            .catch(error => {
                console.log(error)
            })
    },

    ///Clears the translationlist from session and from the database
    async clear(){
        const storedSession = localStorage.getItem('lit-ss')
        const session = (JSON.parse(storedSession))
        session.translations=[]
        await localStorage.setItem('lit-ss',JSON.stringify(session))

        return await fetch(`${apiURL}/translations/${session.id}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new translations to add to user with id 1
                translations: session.translations
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update translations history')
                }
                return response.json()
            })
            .then(updatedUser => {
                return updatedUser
            })
            .catch(error => {
                console.log(error)
            })
    }
}