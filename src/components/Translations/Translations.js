import {Redirect} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import {useState} from "react";
import {translateAttemptAction} from "../../store/actions/translateActions";

const Translations = () => {

    const dispatch = useDispatch()
    const {translation} = useSelector(state => state.translateReducer)

    //useselector

    const {loggedIn,} = useSelector(state => state.sessionReducer)

    const [translationText,setTranslationText] = useState("")
    const [beta,setBeta] = useState(false)

    const onInputChange = event => {
        setTranslationText(event.target.value.replace(/[^a-zA-Z]/g,'')
        )
    }

    /// toggles the instant functionality which uses the translationtext in usestate instead of redux store
    const onBetaClick = () =>{
        setBeta(!beta)
    }

    //dispatches the translateattemptaction which translates the text to sign symbols and stores it in the database
    const onFormSubmit = event =>{
        event.preventDefault() // stop pagereload on formsubmit
        if(translationText!=='') {
            dispatch(translateAttemptAction(translationText))
        }else{
            window.alert("Cannot translate non alphabetic inputs")
        }
    }
    return (
        <>
            {!loggedIn && <Redirect t="/" /> }
            {loggedIn &&
            <AppContainer>
                <div className="mt-3 mb-3 bg-warning">
                    <form onSubmit={onFormSubmit}>
                        <span className="material-icons">keyboard</span>
                        <input type="text" id="translationText"
                               placeholder="Text to translate"
                               className="form-component"
                               onChange={onInputChange}/>
                        <button type="submit" className="btn btn-primary btn-lg">Translate</button>
                    </form>
                    <button onClick={onBetaClick} className={`bg-light rounded-circle border-1 float-end `+(beta? 'bg-warning': '') }>instant</button>
                    <div className="bg-light">
                        {!beta && Array.from(translation).map((c,i) => {
                            return <img key ={i} src = {`LostInTranslation_Resources/individual_signs/${c}.png`} alt={c}/>}) }
                        {beta && Array.from(translationText).map((c,i) => {
                            return <img key ={i} src = {`LostInTranslation_Resources/individual_signs/${c}.png`} alt={c}/>}) }
                    </div>

                </div>
            </AppContainer>
            }
        </>
    )
}

export default Translations