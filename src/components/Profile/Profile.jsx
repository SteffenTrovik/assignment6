import {Redirect} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import {translateClearAttempting} from "../../store/actions/translateActions";


const Profile = () => {

    const dispatch = useDispatch()
    const {loggedIn,translations} = useSelector(state => state.sessionReducer)

    const getTranslations = () => {
        if(translations.length>10)
            return translations.slice(translations.length-10)
        return translations
    }

    /// dispatches the translateclearattempting which removes all translations from the database
    const onClearClick = () =>{
        dispatch(translateClearAttempting())
    }

    return(
        <>
            {!loggedIn && <Redirect to="/" /> }
            {loggedIn &&
            <AppContainer>
                <div>
                    <h2 className="mx-2">Translations</h2>
                    <div className="bg-light">

                        <ol>
                            { getTranslations().map((t,i) => {
                                return <li className="fw-bold" key = {i}>{t} { Array.from(t).map((c,i) => {
                                    return <img width="50px" key ={i} src = {`LostInTranslation_Resources/individual_signs/${c}.png`} alt={c}/>
                                }) }</li>
                            })}
                        </ol>
                        <button className="mx-3" onClick={onClearClick}>Clear translations</button>
                    </div>
                </div>
            </AppContainer>
            }
        </>
    )
}
export default Profile