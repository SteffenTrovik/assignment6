import AppContainer from "../../hoc/AppContainer";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loginAttemptAction} from "../../store/actions/loginActions";
import {Redirect} from "react-router-dom";
import {translateAttemptAction} from "../../store/actions/translateActions";

const Login = () => {

    const dispatch = useDispatch()
    const {loginError, loginAttempting} = useSelector(state => state.loginReducer)
    const {loggedIn} = useSelector(state => state.sessionReducer)

    const [credentials, setCredentials] = useState({
        username: ''
    })

    /// Saves the user input in the credentials username variable
    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value.trim()
        })
    }

    /// dispatches the loginattempt action to try and login/register the user
    const onFormSubmit = event => {
        event.preventDefault() // stop pagereload on formsubmit
        if(credentials.username!=='') {
            dispatch(loginAttemptAction(credentials))
        }else{
            window.alert("Name cannot be empty string")
        }
    }

    return (
        <>
            {loggedIn && <Redirect to="/translations"/>}
            {!loggedIn &&
            <AppContainer>
                <form className="mt-3 mb-3 bg-warning mx-2" onSubmit={onFormSubmit}>
                    <div className="row">
                        <div className="col-2">
                            <img src="LostInTranslation_Resources/Logo.png" width="100px" alt="logo"/>
                        </div>
                        <div className="col-xs-4 col-sm-6">
                            <h1>Lost in Translation <span className="material-icons">thumb_up</span></h1>
                            <h2>Get Started</h2>
                        </div>
                    </div>
                    <span className="material-icons">keyboard</span>
                    <input type="text" id="username"
                           placeholder="What's your name?"
                           className="form-component"
                           onChange={onInputChange}/>
                    <button type="submit" className="btn btn-primary btn-lg">Login</button>
                </form>

                {loginAttempting &&
                <p>Trying to login...</p>
                }

                {loginError &&
                <div className="alert alert-danger" role="alert">
                    <h4>unsuccessful</h4>
                    <p className="mb-0">{loginError}</p>
                </div>
                }
            </AppContainer>
            }
        </>
    )
}
export default Login