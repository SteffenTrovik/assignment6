const apiURL = 'https://sat-noroff-api.herokuapp.com'
const apiKey = 'fHinlLXDHB0GKcGgHurrnlIb1wsrQK8qsYBHUJOdAjjV3V4M6cBRf10Ea5atPn7vCA+IFK97AgQRGmoTWQI9MQ=='

export const LoginAPI = {
    /// Logs the user in, if not registered it registers the user, returns the user
    login(credentials) {
        return fetch(`${apiURL}/translations?username=${credentials.username}`)
            .then(async response => {
                if (!response.ok) {
                    const {error = 'An unknown error occured'} = await response.json()
                    throw new Error(error)
                }
                let data = await response.json()
                if (data.length > 0) {
                    return data[0]
                } else {
                    return await fetch(`${apiURL}/translations`, {
                        method: 'POST',
                        headers: {
                            'X-API-Key': apiKey,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            username: credentials.username,
                            translations: []
                        })
                    })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error('Could not create new user')
                            }
                            return response.json()
                        })
                        .then(newUser => {
                            return newUser
                        })
                }

            })
            .catch(error => {
                console.log(error)
            })
    }

}