import {useDispatch, useSelector} from "react-redux";
import {sessionClearAction} from "../../store/actions/sessionActions";

const Navbar = () => {

    const dispatch = useDispatch()

    const {loggedIn, username} = useSelector(state => state.sessionReducer)

    /// dispatches the sessionclearaction to clear the session which logs the user out and redirects to front page
    const onLogoutClick = () =>{
        dispatch(sessionClearAction())
    }

    return (
        <>
            {!loggedIn &&
            <div className="bg-warning">
                <p className="text-light mx-5 fw-bold">Lost in Translation</p>
            </div>
            }
            {loggedIn &&
            <div className="bg-warning">
                <ul className="list-unstyled navbar mx-5 ">
                    <li><img src="LostInTranslation_Resources/Logo.png" width="50px" alt="logo"/></li>
                    <li className="text-light fw-bold"> Lost in Translation</li>
                    <li><a onClick={onLogoutClick} href="/" className="text-decoration-none text-light nav-link" >Front Page</a></li>
                    <li><a href="/translations" className="text-decoration-none text-light">Translations</a></li>
                    <li><a href="/profile" className="text-decoration-none text-light">{username}</a></li>

                </ul>
            </div>
            }
        </>
    )
}

export default Navbar