# Assignment 6: Create a Sign Language Translator using React
## Lost in Translation
Build an online sign language translator as a Single Page Application using the React framework

## 1) Set up the developement environment
For this app the following tools have been used: Figma, NPM/NODE.js, REACT CRA, Webstorm, Browser dev tools, React and Redux dev tools, git, Rest API: https://github.com/dewald-els/noroff-assignment-api and Heroku

## 2) Design a component tree
Component tree is designed in figma and is added to the repository

## 3) Write HTML & CSS as needed
HTML and CSS is basic, bootstrap classes were used to achieve decent similarity to provided wireframe images.

## 4) Use the React framework to build the following screens into your translator app (See Appendix A for detailed specs):
Three pages were made: Startup page, translation page and profile page.

## Appendix requirements:

### 1) Startup/login page
<img src ="https://i.ibb.co/Mc1pvcz/lofin.png">

First thing users see is a login form, this allows users to log in/register, this automatically checks if users exists and logs them in and if not it registers them. this is using the noroff Translation API. After login users are redirected to the translation page.

### Nav Bar
There is a nav bar that only shows routes when user is logged in, this contains routes for front page which logs users out and clears their session, translation page aswell as the profile page by clicking on the username.

### 2) Translation page
<img src="https://i.ibb.co/xYrPKfC/translations.png">

The translation page allows users to type in text and translate said text upon the click of a button, this is also stored in the database and is rerendered and shown upon translate click. Spaces, numbers and symbols are stripped away and not shown nor stored.

For extra functionality there is a button added on the far right side named instant, this toggles the instant functinality and changes color of button to show its active. Upon clicking this sign symbols are now shown instantly upon typing and are only stored upon clicking the tranlsate button.

### 3) Profile page
<img src="https://i.ibb.co/2y23HsR/profile.png">
The profile page displays the last 10 translations for the current user. It displays both text and the translated symbols. There is also a button that clears the translations from the database.

## 5) Submit
The application is deployedon Heroku and can be accessed with the following link:
https://steffenstranslator.herokuapp.com/




This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
